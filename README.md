# Vayt
Vayt means remote in Yiddish. I have no idea why this needed an edgy name
but there it is.

### Setup
- Start a virtual environment
```
python -m venv .
```

- Activate the environment
```
source bin/activate
```

- Install required packages
```
pip install -r requirements.txt
```

### Creating your hosts file
The host file can take either DNS names or IPs.
Note: In this version I have disabled host cleaning, any host will attempt
to be connected to even if it is not a valid host.
```
cat << '__EOF__' > hosts
host1
host2
host3
...
__EOF__
```


### Running
Create and populate your host file

Final command
```
python vayt.py -f hosts -l $USER
```

$USER is not necessarily needed but if your username on your local machine
you're running this from is not the same as what you are trying to connect
to you'll need the login name flag.
